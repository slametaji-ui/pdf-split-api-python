from fastapi import FastAPI, Form, APIRouter
from pdfrw import PdfReader
import requests
from io import BytesIO

app = FastAPI()

# Initialize router
router = APIRouter()

# Define FastAPI routes
@router.post("/count_pages")
async def count_pages(pdf_url: str = Form(...)):
    # try:
        # Send a GET request to download the PDF from the URL
        response = requests.get(pdf_url, verify=False)
        response.raise_for_status()

        # Create a PdfReader object from the downloaded PDF content
        pdf_data = BytesIO(response.content)
        pdf_reader = PdfReader(pdf_data)

        # Count the number of pages
        page_count = len(pdf_reader.pages)

        return {"status": 200, "message": "Document successfully counted", "page_count": page_count}

    # except requests.exceptions.RequestException as e:
    #     return {"status": 400, "message": "Failed to download the document", "error": str(e)}
    # except Exception as e:
    #     return {"status": 400, "message": "Unable to count pages", "error": str(e)}
