from fastapi import FastAPI, Form, APIRouter
from fastapi.responses import FileResponse
from PyPDF2 import PdfReader
from pdf2image import convert_from_path
from PIL import Image
import requests
import tempfile
import os

app = FastAPI()

# Inisialisasi router
router = APIRouter()

# Definisi rute FastAPI
@router.post("/watermark")
async def watermark_endpoint(
    pdf_url: str = Form(...),
    watermark_image_url: str = Form(...),
):
    try:
        # Tambahkan watermark gambar ke PDF
        output_path = "watermarked.pdf"
        pdf_response = requests.get(pdf_url, verify=False)
        pdf_response.raise_for_status()
        pdf_data = pdf_response.content

        # Unduh gambar watermark dari URL
        watermark_response = requests.get(watermark_image_url, verify=False)
        watermark_response.raise_for_status()
        watermark_data = watermark_response.content

        # Simpan file PDF sementara
        with tempfile.NamedTemporaryFile(delete=False, suffix=".pdf") as pdf_temp_file:
            pdf_path = pdf_temp_file.name
            pdf_temp_file.write(pdf_data)

            # Membaca file PDF
            pdf_reader = PdfReader(open(pdf_path, "rb"))
            pdf_writer = PdfWriter()

            # Konversi PDF ke gambar (PNG)
            images = convert_from_path(pdf_path)

            for i, page in enumerate(pdf_reader.pages):
                image = images[i]

                # Buka gambar watermark
                watermark = Image.open(io.BytesIO(watermark_data))

                # Tambahkan watermark ke gambar
                watermark.paste(image, (0, 0), image)
                watermark.save(f"temp_page_{i}.pdf")

                # Baca kembali gambar yang telah diberi watermark
                page_with_watermark = PdfReader(open(f"temp_page_{i}.pdf", "rb")).getPage(0)

                # Tambahkan halaman yang telah diberi watermark ke file PDF
                pdf_writer.addPage(page_with_watermark)

                # Hapus file gambar sementara
                os.remove(f"temp_page_{i}.pdf")

            # Simpan hasil ke file PDF baru
            with open(output_path, "wb") as output_file:
                pdf_writer.write(output_file)

        # Hapus file sementara
        os.remove(pdf_path)

        return FileResponse(output_path, headers={"Content-Disposition": "attachment; filename=watermarked.pdf"})

    except Exception as e:
        return {"status": 400, "message": "Terjadi kesalahan dalam pemrosesan file"}

app.include_router(router)
