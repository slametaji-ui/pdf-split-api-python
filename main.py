from fastapi import FastAPI
from pdf_split_api import pdf_split
from pdf_count_pages_api import pdf_count_pages
from pdf_watermark_api import pdf_watermark

app = FastAPI()

app.include_router(pdf_split.router)
app.include_router(pdf_count_pages.router)
app.include_router(pdf_watermark.router)