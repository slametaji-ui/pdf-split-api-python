from fastapi import FastAPI, Form, APIRouter
import PyPDF2
import requests
from io import BytesIO
import base64

app = FastAPI()
router = APIRouter()


def fetch_pdf_data(pdf_url):
    try:
        response = requests.get(pdf_url, verify=False)
        response.raise_for_status()
        return BytesIO(response.content)
    except requests.exceptions.RequestException as e:
        return None


def split_pdf_sections(pdf_data, start_page, end_page):
    pdf_reader = PyPDF2.PdfReader(pdf_data)
    pdf_writer = PyPDF2.PdfWriter()

    for page_num in range(start_page - 1, end_page):
        pdf_writer.add_page(pdf_reader.pages[page_num])

    return pdf_writer


def encode_pdf_base64(pdf_writer):
    response_content = BytesIO()
    pdf_writer.write(response_content)
    response_content.seek(0)
    return base64.b64encode(response_content.read()).decode('utf-8')


@router.post("/split_pdf")
async def split_pdf(
    front_start_page: int = Form(...),
    front_end_page: int = Form(...),
    kb_page_ranges: str = Form(...),
    back_start_page: int = Form(...),
    back_end_page: int = Form(...),
    pdf_url: str = Form(...),
    nama_mata_kuliah: str = Form(...),
    nama_modul: str = Form(...),
):
    pdf_data = fetch_pdf_data(pdf_url)

    if not pdf_data:
        return {"status": 400, "message": "Dokumen tidak ditemukan"}

    response_contents = {
        "front": split_pdf_sections(pdf_data, front_start_page, front_end_page),
        "kb": [],
        "back": split_pdf_sections(pdf_data, back_start_page, back_end_page),
    }

    # Split the "kb" page ranges
    kb_page_ranges_list = kb_page_ranges.split(',')

    for kb_range in kb_page_ranges_list:
        start, end = map(int, kb_range.split('-'))
        kb_section = split_pdf_sections(pdf_data, start, end)
        response_contents["kb"].append(kb_section)

    pdf_base64 = {
        "front": encode_pdf_base64(response_contents["front"]),
        "kb": [encode_pdf_base64(section) for section in response_contents["kb"]],
        "back": encode_pdf_base64(response_contents["back"]),
    }

    output_file_names = {
        "front": f'{nama_mata_kuliah}_MODUL{nama_modul}_CVR_DFTRISI_PNDHLN.pdf',
        "kb": [f'{nama_mata_kuliah}_MODUL{nama_modul}_KB{i + 1}.pdf' for i in range(len(response_contents["kb"]))],
        "back": f'{nama_mata_kuliah}_MODUL{nama_modul}_KJTF_GLSRM_DFTRPSTK.pdf',
    }

    response_data = {
        "status": 200,
        "message": "File berhasil disimpan",
        "title": {
            "front": output_file_names["front"],
            "kb": output_file_names["kb"],
            "back": output_file_names["back"]
        },
    }

    response_data["front_base64"] = pdf_base64["front"]
    response_data["kb_base64"] = pdf_base64["kb"]
    response_data["back_base64"] = pdf_base64["back"]

    return response_data
