FROM python:3.11

COPY . /app

WORKDIR /app

RUN pip install fastapi uvicorn[standard] PyPDF2 pdfrw requests python-multipart pdf2image pycryptodome validators

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
